#!/usr/bin/env bash

cat settings.yaml.tmpl | envsubst >settings.yaml

RUST_LOG=DEBUG ./scalpel 2>run.log &

export CLIENT_PID
CLIENT_PID=$!
echo "Client started at pid $CLIENT_PID"
